extern crate capnpc;

fn main() {
    ::capnpc::CompilerCommand::new()
        .file("capnp/universe.capnp")
        .run()
        .unwrap();
}
