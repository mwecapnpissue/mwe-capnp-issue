use async_std::net::{TcpListener, TcpStream};
use async_std::prelude::*;
use async_std::task;
use capnp::capability::Promise;
use capnp::Error;
use capnp_rpc::{rpc_twoparty_capnp, twoparty, RpcSystem};
use futures::AsyncReadExt;
use futures_intrusive::sync::ManualResetEvent;
mod universe_capnp {
    include!(concat!(env!("OUT_DIR"), "/capnp/universe_capnp.rs"));
}

struct Server {}
impl universe_capnp::universe::Server for Server {
    fn get_meaning(
        &mut self,
        _params: universe_capnp::universe::GetMeaningParams,
        mut results: universe_capnp::universe::GetMeaningResults,
    ) -> Promise<(), Error> {
        results.get().set_meaning(42);
        Promise::ok(())
    }
}

fn main() {
    const ADDRESS: &'static str = "127.0.0.1:9291";
    let server_started1 = std::sync::Arc::new(ManualResetEvent::new(false));
    let server_started2 = server_started1.clone();
    std::thread::spawn(move || {
        task::block_on(async move {
            let listener = TcpListener::bind(ADDRESS).await.unwrap();
            server_started1.set();
            let mut incoming = listener.incoming();
            while let Some(socket) = incoming.next().await {
                let socket = socket.unwrap();
                socket.set_nodelay(true).unwrap();
                let client = universe_capnp::universe::ToClient::new(Server {})
                    .into_client::<capnp_rpc::Server>();
                let (reader, writer) = socket.split();
                let network = twoparty::VatNetwork::new(
                    reader,
                    writer,
                    rpc_twoparty_capnp::Side::Server,
                    Default::default(),
                );
                let rpc_system = RpcSystem::new(Box::new(network), Some(client.clone().client));
                rpc_system.await.unwrap();
            }
        });
    });

    task::block_on(async move {
        server_started2.wait().await;
        println!("Starting...");
        let socket = TcpStream::connect(ADDRESS).await.unwrap();
        socket.set_nodelay(true).unwrap();
        let (reader, writer) = socket.split();
        let network = twoparty::VatNetwork::new(
            reader,
            writer,
            rpc_twoparty_capnp::Side::Client,
            Default::default(),
        );
        let mut rpc_system = RpcSystem::new(Box::new(network), None);
        let client: universe_capnp::universe::Client =
            rpc_system.bootstrap(rpc_twoparty_capnp::Side::Server);
        let request = client.get_meaning_request();
        let result = request
            .send()
            .promise
            .await
            .unwrap()
            .get()
            .unwrap()
            .get_meaning();
        println!("{}", result);
    });
}
